let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')

fetch(`https://gentle-lowlands-85183.herokuapp.com/api/courses/${courseId}`, {
    method: 'POST',
    headers: {
        'Authorization': `Bearer ${token}`
    }
})
    .then(res => res.json())
    .then(data => {
        console.log(data) //true 


        if (data === true) {
            window.location.replace('./courses.html')
        } else {
            alert('something went wrong')
        }
    })