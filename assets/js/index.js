let navItems = document.querySelector("#navSession");
let registerLink = document.querySelector("#register");
let profilePageLink = document.querySelector("#profilePage")
let userToken = localStorage.getItem("token");
// console.log('aaaa', profilePageLink)
if (!userToken) {
	navItems.innerHTML =
		`
			<li class="nav-item ">
				<a href="./pages/login.html" class="nav-link"> Login </a>
			</li>
		`
	registerLink.innerHTML =
		`
			<li class="nav-item ">
				<a href="./pages/register.html" class="nav-link"> Register </a>
			</li>
		`
} else {
	profilePageLink.innerHTML =
		`
		<li class="nav-item ">
			<a href="./pages/profilePage.html" class="nav-link"> Profile </a>
		</li>
		`



	navItems.innerHTML =
		`
			<li class="nav-item ">
				<a href="./pages/logout.html" class="nav-link"> Log Out </a>
			</li>
		`


}

//popover
$(document).ready(function () {
	$('[data-toggle="popover"]').popover({
		placement: 'bottom',
		trigger: 'hover'
	});
});


