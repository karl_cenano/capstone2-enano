/* ------------------------------ declarations ------------------------------ */

let token = localStorage.getItem('token')
let userId = localStorage.getItem('id')
let changePasswordForm = document.querySelector('#changePasswordForm')
let fieldreq = document.querySelectorAll("span.small")


/* -------------------------------- functions ------------------------------- */

changePasswordFunc = async (token, id, password) => {

    try {

        let fetchPassword = await fetch(`https://gentle-lowlands-85183.herokuapp.com/api/users/details/changePassword`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: id,
                password: password
            })
        })

        let fetchPasswordJSON = await fetchPassword.json();
        // console.log('fetchjson', fetchPasswordJSON)
        return fetchPasswordJSON

    } catch (err) {
        return alert('Error something went wrong')
    }

}

FieldReqDisplayError = (inputField, messageSpan) => {
    if (inputField.length == 0) {
        messageSpan.classList.remove("d-none");
        return true

    } else {
        messageSpan.classList.add("d-none");
        return false
    }
}

/* ----------------------------- event listener ---------------------------- */

changePasswordForm.addEventListener('submit', (e) => {
    let newPassword = document.querySelector('#changePassword1').value
    let verifyPassword = document.querySelector('#changePassword2').value

    let passErr = FieldReqDisplayError(newPassword, fieldreq[0])
    let verPassErr = FieldReqDisplayError(verifyPassword, fieldreq[1])

    e.preventDefault()

    if (passErr == false && verPassErr == false && newPassword == verifyPassword) {

        changePasswordFunc(token, userId, newPassword).then(passwordChanged => {
            if (passwordChanged == true) {
                alert(`Password has been changed`)
                window.location.replace('./profilePage.html')
            } else {
                alert('Error something went wrong')
            }
        })
    } else {
        alert('Password do not match try again')
    }
})

