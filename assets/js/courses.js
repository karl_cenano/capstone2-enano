/* ------------------------------ declarations ------------------------------ */

let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin") === "true"
let addButton = document.querySelector("#adminButton")
let cardFooter;
let container = document.querySelector("#coursesContainer")


//This if statement will allow us to show a button for adding a course;a button to redirect us to the addCourse page if the user is admin, however, if he/she is a guest or a regular user, they should not see a button.

if (adminUser === false || adminUser === null) {
	addButton.innerHTML = null
} else {
	addButton.innerHTML = `
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
		</div>
	`
}


/* ---------------------------------- fetch --------------------------------- */

//! 3 ANCHOR is Active displays card
fetch('https://gentle-lowlands-85183.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		// console.log(data)
		// A variable that will store the data to be rendered
		let courseData;
		// If the number of courses is less than 1, display no courses available
		if (data.length < 1) {
			courseData = "No courses available";
		} else {
			counter = 0

			courseData = data.map(course => {
				// console.log('course.enrollees', course.enrollees);
				counter++;
				let alreadyEnrolledElement = `<span class="font-weight-bold text-success">Status: Available</span>`;
				let courseMapId = course.enrollees.map((courseMap) => { //! scale
					// console.log('courseMap.userId', courseMap.userId)
					return courseMap.userId
				})

				if (courseMapId.includes(localStorage.id)) {
					// console.log('true')
					alreadyEnrolledElement = `<span class="font-weight-bold text-danger">Status: Enrolled</span>`
				} else {
					// console.log('false')
					alreadyEnrolledElement = `<span class="font-weight-bold text-success">Status: Available</span>`
				}


				if (adminUser == false || !adminUser) {
					//*user
					if (course.isActive == true) {
						cardFooter =
							`
								${alreadyEnrolledElement}
								<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton">
									Select Course
								</a>
								
							`
						return (
							`
								<div class="col-md-6 my-3 p-2">
									<div class="card">
										<div class="card-body style="padding-bottom: 50px">
											<h5 class="card-title">${course.name}</h5>
											<p class="card-text text-left">
												${course.description}
											</p>
											<p class="card-text text-right">
												₱ ${course.price}
											</p>

										</div>
										<div class="card-footer">
										${cardFooter}
										</div>
									</div>
								</div>
							`
						)
					}
				} else {
					//*admin				
					// console.log('course.isActive', course.isActive)
					if (course.isActive == false) {
						// console.log('disable')
						cardFooter =
							`
						<a href="./enableCourse.html?courseId=${course._id}" value={course._id} class="btn btn-success text-white btn-block deleteButton">
							Enable
						</a>
						<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton">
								View Course
						</a>
						`
					} else {
						// isActive = true
						// console.log('enable')
						cardFooter =
							`
						<a href="./disableCourse.html?courseId=${course._id}" value={course._id} class="btn btn-dark text-white btn-block deleteButton">
							Disable
						</a>
						<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton">
								View Course
							</a>
						`

					}
					return (
						`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">${course.name}</h5>
									<p class="card-text text-left">
										${course.description}
									</p>
									<p class="card-text text-right">
										₱ ${course.price}
									</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
					)
				}
			}).join("")

		}



		container.innerHTML = courseData;

		if (adminUser === true) {
			fetch('https://gentle-lowlands-85183.herokuapp.com/api/users', {
				headers: { 'Authorization': `Bearer ${token}` }
			})
				.then(res => res.json())
				.then(data => {
					let usersRegisteredHeader = document.querySelector('#usersRegisteredHeader')
					usersRegisteredHeader.innerHTML = `Users Enrolled`
					let UserMap = data.map(result => {
						if (result.isAdmin !== true) {
							return (
								`
								${result.firstName} ${result.lastName}
								<br>
							`
							)
						}

					}).join("")
					let usersRegistered = document.querySelector("#usersRegistered")
					usersRegistered.innerHTML = UserMap
				})
		}
	})

