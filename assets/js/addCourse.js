let formSubmit = document.querySelector("#createCourse")
let fieldreq = document.querySelectorAll("span.small")
let token = localStorage.getItem("token")
let courseNameExist = [];
let alreadyExist = document.querySelector("#alreadyExists")

fetch('https://gentle-lowlands-85183.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		// console.log(data)
		data.map((course) => {
			// console.log(course.name)
			courseNameExist.push(course.name)
		})
		// console.log(courseNameExist)

		//add an event listener:
		formSubmit.addEventListener("submit", (e) => {
			e.preventDefault()

			//get the values of your input:
			let courseName = document.querySelector("#courseName").value
			let description = document.querySelector("#courseDescription").value
			let price = document.querySelector("#coursePrice").value

			let courseNameHasErr = FieldReqDisplayError(courseName, fieldreq[0])
			let descriptionHasErr = FieldReqDisplayError(description, fieldreq[1])
			let priceHasErr = FieldReqDisplayError(price, fieldreq[2])

			function FieldReqDisplayError(inputField, messageSpan) {
				if (inputField.length == 0) {
					messageSpan.classList.remove("d-none");
					return true
				} else {
					messageSpan.classList.add("d-none");
					return false
				}
			}

			let includesCourse = courseNameExist.includes(courseName)

			if (includesCourse) {
				alreadyExist.innerHTML = `Course Name already exists`
			}

			//Create a fetch request to add a new course:
			if (courseName.length != 0 && description.length != 0 && price.length != 0 && !includesCourse) {
				fetch('https://gentle-lowlands-85183.herokuapp.com/api/courses', {

					method: 'POST',
					headers: {

						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`

					},
					body: JSON.stringify({
						name: courseName,
						description: description,
						price: price
					})

				})
					.then(res => res.json())
					.then(data => {
						if (data === true) {
							alert('Course Created')
							window.location.replace('./courses.html')
						} else {
							alert("Course Creation Failed. Something Went Wrong.")
						}
					})
			}
		})

	})










