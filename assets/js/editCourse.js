let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')
let fieldreq = document.querySelectorAll("span.small")

fetch(`https://gentle-lowlands-85183.herokuapp.com/api/courses/${courseId}`)
    .then(res => res.json())
    .then(data => {
        // console.log('aaaaa', data)

        let courseNameElement = document.querySelector('#courseName')
        let coursePriceElement = document.querySelector('#coursePrice')
        let courseDescriptionElement = document.querySelector('#courseDescription')
        let courseForm = document.querySelector("#editCourse");

        courseNameElement.value = data.name
        coursePriceElement.value = data.price
        courseDescriptionElement.value = data.description


        courseForm.addEventListener("submit", (e) => {

            e.preventDefault()

            let courseName = document.querySelector("#courseName").value
            let coursePrice = document.querySelector("#coursePrice").value;
            let description = document.querySelector("#courseDescription").value;

            let courseNameHasErr = FieldReqDisplayError(courseName, fieldreq[0])
            let descriptionHasErr = FieldReqDisplayError(description, fieldreq[2])
            let priceHasErr = FieldReqDisplayError(coursePrice, fieldreq[1])

            function FieldReqDisplayError(inputField, messageSpan) {
                if (inputField.length == 0) {
                    messageSpan.classList.remove("d-none");
                    return true
                } else {
                    messageSpan.classList.add("d-none");
                    return false
                }
            }

            if (courseName.length != 0 && description.length != 0 && coursePrice.length != 0) {
                fetch('https://gentle-lowlands-85183.herokuapp.com/api/courses/', {
                    method: 'PUT',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        id: courseId,
                        name: courseName,
                        description: description,
                        price: coursePrice
                    })
                })
                    .then(res => res.json())
                    .then(data => {
                        // console.log(data);
                        // console.log(data.name);
                        // console.log(data.description);
                        // console.log(data.price);

                        if (data) {
                            alert("Course Updated")
                            window.location.replace("courses.html")

                        } else {

                            alert("something went wrong")
                        }
                    })
            }

        })
    })