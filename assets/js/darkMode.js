//<input type="checkbox" id="switch" /><label for="switch">Toggle</label>
//localStorage.setItem('token', data.accessToken)
let darkMode = localStorage.getItem('darkMode');

const darkModeToggle = document.querySelector('#dark-mode-toggle');
const darkCircle = document.querySelectorAll('.bg-circle')[0]
const darkCircle1 = document.querySelectorAll('.bg-circle')[1]
const darkCircle2 = document.querySelectorAll('.bg-circle')[2]
const darkCircle3 = document.querySelectorAll('.bg-circle')[3]

const headerContainer = document.querySelector('header.masthead')
const headerButton = document.querySelector('.rounded-pill')
console.log(darkCircle)



const enableDarkMode = () => {
    // 1. Add the class to the body
    document.body.classList.add('darkmode');
    headerContainer.classList.add('darkmode');
    headerButton.classList.add('darkmode');
    darkCircle.classList.add('darkmode')
    darkCircle1.classList.add('darkmode')
    darkCircle2.classList.add('darkmode')
    darkCircle3.classList.add('darkmode')
    // 2. Update darkMode in localStorage
    localStorage.setItem('darkMode', 'enabled');
}

const disableDarkMode = () => {
    // 1. Remove the class from the body
    document.body.classList.remove('darkmode');
    headerContainer.classList.remove('darkmode')
    headerButton.classList.remove('darkmode');
    darkCircle.classList.remove('darkmode')
    darkCircle1.classList.remove('darkmode')
    darkCircle2.classList.remove('darkmode')
    darkCircle3.classList.remove('darkmode')
    // 2. Update darkMode in localStorage 
    localStorage.setItem('darkMode', null);
}

// If the user already visited and enabled darkMode
// start things off with it on
if (darkMode === 'enabled') {
    enableDarkMode();
}

// When someone clicks the button
darkModeToggle.addEventListener('click', () => {
    // get their darkMode setting
    darkMode = localStorage.getItem('darkMode');

    // if it not current enabled, enable it
    if (darkMode !== 'enabled') {
        enableDarkMode();
        // if it has been enabled, turn it off  
    } else {
        disableDarkMode();
    }
});


