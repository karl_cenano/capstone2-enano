/* ------------------------------ declarations ------------------------------ */
let token = localStorage.getItem('token')
let fullNameElement = document.querySelector('#fullName');
let mobileNoElement = document.querySelector('#mobileNo');
let enrollmentsElement = document.querySelector('#enrollments');
let userId = localStorage.getItem('id')
let editButtonUser = document.querySelector('#editButtonUser');
/* ---------------------------------- fetch --------------------------------- */


// firstname , mobile number , courses enrollments ----> courses name
// depends on the person who logged in
// look at database or i will just look at the schema
// course details(enrolled, status, courseId)

fetch('https://gentle-lowlands-85183.herokuapp.com/api/users/details', {
    method: 'GET',
    headers: {
        Authorization: 'Bearer ' + token
    }
}
)
    .then(res => res.json())
    .then(resultNgFetch => {
        //^ REVIEW important
        editButtonUser.innerHTML = `<a href="./editProfile.html?userId=${userId}" value={user._id} class="btn btn-outline-danger editButton mt-3 mb-3">
        Edit Profile
    </a>`


        //console.log('aaaaaaaaaaaa', resultNgFetch) //properties and methods ng resultNgFetch
        fullNameElement.innerHTML =
            `
            ${resultNgFetch.firstName.charAt(0).toUpperCase() + resultNgFetch.firstName.slice(1)} 
            ${resultNgFetch.lastName.charAt(0).toUpperCase() + resultNgFetch.lastName.slice(1)} 
            `

        mobileNoElement.innerHTML = 'mobile number: ' + resultNgFetch.mobileNo

        // console.log('bbbbbbbbb', resultNgFetch.enrollments)
        if (resultNgFetch.enrollments.length > 0) {
            let enrollmentsTable = resultNgFetch.enrollments.map(enrollments => {
                // console.log('enroll', enrollments)
                fetch(`https://gentle-lowlands-85183.herokuapp.com/api/courses/${enrollments.courseId}`)
                    .then(res => res.json())
                    .then(data => {
                        //console.log('dat11111a', data)

                        if (data) {
                            enrollmentsElement.innerHTML += //! ANCHOR really important 
                                `
                                <div class="col-12 my-3">
                                    <div class="card">
                                        <div class="card-body ">
                                            <h2 class="card-title">${data.name}</h2>
                                            <h5 class="card-text"> enrolled on ${enrollments.enrolledOn}</h5>
                                            <h5 class="card-text"> status: ${enrollments.status}</h5>
                                        </div>
                                    </div>
                                </div>
                                `



                        } else {
                            alert("Something went wrong")
                        }
                    })

            })
        }
    })

