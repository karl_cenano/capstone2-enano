let navItems = document.querySelector("#navSession");
let registerLink = document.querySelector("#register");
let profilePageLink = document.querySelector("#profilePage")
let userToken = localStorage.getItem("token");

if (!userToken) {
	navItems.innerHTML =
		`
			<li class="nav-item ">
				<a href="./login.html" class="nav-link"> Login </a>
			</li>
		`
	registerLink.innerHTML =
		`
			<li class="nav-item ">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
		`
} else {
	profilePageLink.innerHTML =
		`
		<li class="nav-item ">
			<a href="./profilePage.html" class="nav-link"> Profile </a>
		</li>
		`
	navItems.innerHTML =
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Log Out </a>
			</li>
		`
}


//popover
$(document).ready(function () {
	$('[data-toggle="popover"]').popover({
		placement: 'bottom',
		trigger: 'hover'
	});
});