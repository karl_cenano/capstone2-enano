let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
console.log(courseId)
let token = localStorage.getItem('token')
console.log(token)
fetch(`https://gentle-lowlands-85183.herokuapp.com/api/courses/${courseId}`, {
    method: 'DELETE',
    headers: {
        'Authorization': `Bearer ${token}`
    }
})
    .then(res => res.json())
    .then(data => {
        console.log(data) //true 


        if (data === true) {
            // window.location.replace('./courses.html')
        } else {
            alert('something went wrong')
        }
    })
