// /course.html?courseId=60236f7fa57a8724b07d0457
//URL Query parameters
/*
	?courseId=60236f7fa57a8724b07d0457

	? - start of query string
	courseId= (parameter name)
	60236f7fa57a8724b07d0457 = value

*/
//window.location.search return the query string in the URL
//console.log(window.location.search)

//instatiate or create a new URLSearchParams object.
//This object, URLSearchParams, is used an interface to gain access to methods that allow us to specific parts of the query string.
let params = new URLSearchParams(window.location.search)

//The has method for URLSearchParams checks if the courseId key exists in our URL Query string.
//console.log(params.has('courseId'))//true, if the there is a key

//The get method for URLSearchParams returns the value of the key passed in as an argument.
//console.log(params.get('courseId'))

//store the courseID from the URL Query string in a variable:
let courseId = params.get('courseId')
//get the token from localStorage
let token = localStorage.getItem('token')


let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")
let enrolleesElement = document.querySelector("#enrollees")
let adminUser = localStorage.getItem("isAdmin") === "true"
let alreadyEnrolled = document.querySelector("#alreadyEnrolled")
let listOfEnrollees = document.querySelector("#listOfEnrollees")
let totalEnrolled = document.querySelector("#totalEnrolled")
let editButton = document.querySelector("#editButton")
let spinnerEnroll = document.querySelector("#spinnerEnroll")


//Get the details of a single course.
// api/courses/:id
fetch(`https://gentle-lowlands-85183.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {

		courseName.innerHTML = data.name
		courseDesc.innerHTML = data.description
		coursePrice.innerHTML = data.price

		//! 
		if (!adminUser) {
			enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

			document.querySelector("#enrollButton").addEventListener("click", () => {
				spinnerEnroll.innerHTML = `<span class="spinner-border text-primary mb-2"></span>`
				//add fetch request to enroll our user:
				fetch('https://gentle-lowlands-85183.herokuapp.com/api/users/enroll', {

					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						courseId: courseId
					})

				})
					.then(res => res.json())
					.then(data => {
						//redirect the user to the courses page after enrolling.
						if (token) {
							if (data === true) {
								//show an alert to thank the user:
								alert('Thank you for enrolling to the course.')
								window.location.replace('./courses.html')
							} else {
								//server error while enrolling to course:
								spinnerEnroll.innerHTML = ``
								alreadyEnrolled.innerHTML = `
								<div class="text-danger">
								<h2>Error. User is Already Enrolled!</h2>
								</div>
								`
							}
						} else {
							window.location.replace('./register.html')
						}

					})
			})
		}
		if (adminUser == true) {
			editButton.innerHTML = `<a href="./editCourse.html?courseId=${courseId}" value={course._id} class="btn btn-primary text-white btn-block editButton mt-3 mb-1">
			Edit
		</a>`
			let counter = 0
			let enrolleesTable = data.enrollees.map(enrollees => {
				let enrolleesId = enrollees.userId
				fetch(`https://gentle-lowlands-85183.herokuapp.com/api/users/${enrolleesId}`)
					.then(res => res.json())
					.then(data => {
						// console.log('dataOfenrolleesTable', data)

						if (data) {
							counter++;
							totalEnrolled.innerHTML = `<h4>Total Enrolled: ${counter} </h3>`
							enrolleesElement.innerHTML += //! really important 
								`
								<li>${data.firstName}  ${data.lastName}</li> 
								
								`
						} else {
							alert("Something went wrong")
						}
					})
			})
			listOfEnrollees.innerHTML = `List of Enrollees`
		}
	})



