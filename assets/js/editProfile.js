let userId = localStorage.getItem('id')
let token = localStorage.getItem('token')
let fieldreq = document.querySelectorAll("span.small")
let spinner = document.querySelector("#spinner");


// console.log(fieldreq)
fetch(`https://gentle-lowlands-85183.herokuapp.com/api/users/details`, {
    method: "GET",
    headers: {
        'Authorization': `Bearer ${token}`
    }
})
    .then(res => res.json())
    .then(data => {
        // console.log('aaaaa', data)

        let userFirstNameElement = document.querySelector('#userFirstName')
        let userLastNameElement = document.querySelector('#userLastName')
        let userMobileNumberElement = document.querySelector('#userMobileNumber')
        let userForm = document.querySelector("#editUser");

        userFirstNameElement.value = data.firstName
        userLastNameElement.value = data.lastName
        userMobileNumberElement.value = data.mobileNo



        function FieldReqDisplayError(inputField, messageSpan) {
            if (inputField.length == 0) {
                messageSpan.classList.remove("d-none");
                // console.log('hello')
                return true
            } else {
                messageSpan.classList.add("d-none");
                return false
            }
        }
        function FieldReqDisplayErrorForMobileNo(inputField, messageSpan) {
            if (inputField.length != 11) {
                messageSpan.classList.remove("d-none");
                return true
            } else {
                messageSpan.classList.add("d-none");
                return false
            }
        }




        userForm.addEventListener("submit", (e) => {

            FieldReqDisplayError(userFirstNameElement.value, fieldreq[0])
            FieldReqDisplayError(userLastNameElement.value, fieldreq[1])
            FieldReqDisplayErrorForMobileNo(userMobileNumberElement.value, fieldreq[2])

            e.preventDefault()
            if (userFirstNameElement.value.length != 0 && userLastNameElement.value.length != 0 && userMobileNumberElement.value.length == 11) {
                let userFirstName = document.querySelector("#userFirstName").value
                let userLastName = document.querySelector("#userLastName").value;
                let userMobileNo = document.querySelector("#userMobileNumber").value;

                spinner.innerHTML = `<span class="spinner-border text-primary"></span>`
                fetch('https://gentle-lowlands-85183.herokuapp.com/api/users/details', {
                    method: 'PUT',
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        id: userId,
                        firstName: userFirstName,
                        lastName: userLastName,
                        mobileNo: userMobileNo
                    })
                })
                    .then(res => res.json())
                    .then(data => {

                        // console.log(data);
                        // console.log(data.firstName)
                        // console.log(data.lastName)
                        // console.log(data.mobileNo)
                        // console.log(data.password)
                        if (data) {
                            alert("User Updated")
                            window.location.replace("profilePage.html")

                        } else {

                            alert("something went wrong")
                        }
                    })
            }

        })


    })