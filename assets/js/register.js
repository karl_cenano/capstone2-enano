let registerForm = document.querySelector("#registerUser")
let fieldreq = document.querySelectorAll("span.small")
let spinner = document.querySelector("#spinner");
let emailExistElement = document.querySelector("#emailExist")
// console.log(emailExistElement)

document.querySelector("#userEmail").addEventListener("keypress", (e) => {
	emailExistElement.classList.add("d-none");
})
//Submit event - allows us to submit a form.
//It's default behavior is that it sends your form and refreshes the page
registerForm.addEventListener("submit", (e) => {

	//e = event object. This event object pertains to the event and where it was triggered.
	//preventDefault() = prevents the submit event of its default behavior



	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value
	let email = document.querySelector("#userEmail").value

	function FieldReqDisplayError(inputField, messageSpan) {
		if (inputField.length == 0) {
			messageSpan.classList.remove("d-none");
			return true

		} else {
			messageSpan.classList.add("d-none");
			return false
		}
	}

	function FieldReqDisplayErrorForMobileNo(inputField, messageSpan) {
		if (inputField.length != 11) {
			messageSpan.classList.remove("d-none");
			return true
		} else {
			messageSpan.classList.add("d-none");
			return false
		}
	}

	FieldReqDisplayError(firstName, fieldreq[0])
	FieldReqDisplayError(lastName, fieldreq[1])
	FieldReqDisplayErrorForMobileNo(mobileNo, fieldreq[2])
	FieldReqDisplayError(email, fieldreq[3])
	FieldReqDisplayError(password1, fieldreq[5])
	FieldReqDisplayError(password2, fieldreq[6])
	e.preventDefault()

	if ((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)) {
		spinner.innerHTML = `<span class="spinner-border text-primary"></span>`
		// fetch( url , options )
		fetch('https://gentle-lowlands-85183.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email
			})
			// "{email: email}"
		})
			.then(res => res.json())
			.then(data => {

				// console.log(data)

				if (data === false) {
					fetch('https://gentle-lowlands-85183.herokuapp.com/api/users', {
						method: 'POST',
						headers: { 'Content-Type': 'application/json' },
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							password: password1,
							mobileNo: mobileNo
						})
					})
						.then(res => res.json())
						.then(data => {

							// console.log(data);

							if (data === true) {
								alert("registered successfully")

								//redirect to login page
								window.location.replace("./login.html")
							} else {
								// error in creating registration
								alert("something went wrong")

							}

						})

				} else {
					alert('user already exist')
					spinner.innerHTML = ``
					emailExistElement.classList.remove("d-none")
				}

			})

	}

})